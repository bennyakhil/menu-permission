<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
 use Illuminate\Database\Eloquent\Model;
 use Illuminate\Support\Facades\DB;
use App\Role;
use App\User;
use App\UserRole;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(Gate::denies('register'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $user = User::all();
        return view('user.list', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies('register'), Response::HTTP_FORBIDDEN, '403 Forbidden');
         $role = Role::all();
        return view('user.creat', compact('role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_if(Gate::denies('register'), Response::HTTP_FORBIDDEN, '403 Forbidden');
             $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
       if ($user->save()) {
        $user_role = new UserRole;
         $user_role->user_id = $user->id;
         $user_role->role_id = $request->role;
         $user_role->save();
        }
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort_if(Gate::denies('register'), Response::HTTP_FORBIDDEN, '403 Forbidden');
       //$user = User::findOrFail(decrypt($id));
        $d_id=decrypt($id);
       $user = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->select('users.*', 'role_user.role_id')
            ->where('users.id',$d_id)
            ->first();
            $role = Role::all();
         $data = UserRole::where([['user_id',decrypt($id)]])->get();
        return view('user.edit', compact('role','user','data'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort_if(Gate::denies('register'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
       if ($user->save()) {
        UserRole::where([['user_id',$user->id]])->delete();
        $user_role = new UserRole;
         $user_role->user_id = $user->id;
         $user_role->role_id = $request->role;
         $user_role->save();
        }
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort_if(Gate::denies('register'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        User::destroy($id);
    }
}
