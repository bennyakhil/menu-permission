<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use App\PermissionRole;
use App\Permission;
use App\Role;







class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::all();
        return view('roles.list', compact('role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies('role'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $permissions = Permission::all();

        return view('roles.creat', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_if(Gate::denies('role'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $role = new Role;
        $role->title = $request->name;
        //dd($request->name);
        if($role->save()){
           $permissions=$request->permissions;
        for($i=0;$i<count($permissions);$i++) {
            
            $permission = new PermissionRole;
            $permission->role_id = $role->id;
            $permission->permission_id = $permissions[$i];
            $permission->save();
        }
        
            
            
        }
        

        return redirect()->route('roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort_if(Gate::denies('role'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $role = Role::findOrFail(decrypt($id));
         $permissions = Permission::all();
         $data = PermissionRole::where([['role_id',decrypt($id)]])->get();
        return view('roles.edit', compact('role','permissions','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $role = Role::findOrFail($id);
        $role->title = $request->name;
        if($role->save()){
           $permissions=$request->permissions;
           PermissionRole::where([['role_id',$id]])->delete();
        for($i=0;$i<count($permissions);$i++) {
            
            $permission = new PermissionRole;
            $permission->role_id = $id;
            $permission->permission_id = $permissions[$i];
            $permission->save();
        }  
        }
        return redirect()->route('roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        Role::destroy($id);
        PermissionRole::where([['role_id',$id]])->delete();
        return redirect()->route('roles.index');
    }
}
